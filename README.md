# Proyectos de Sistemas Distribuidos Sistema Chat
Proyecto Chat para la materia de sistemas distribuidos cuenta con un servidor transaccional que es el encargado de gestionar las conexión con los diferentes clientes y contiene la lógica de la aplicación. Se tiene un cliente liviano donde se podrán enviar mensajes y ejecutar los comandos básicos.

## Autores

* Paola Andrea Betancurt
* Juan David Abril

## Servidor

El servidor chat es un servidor transaccional multi hilo desarrollado en java compuesto por los siguientes paquetes:

![servidor-paquete.png](https://bitbucket.org/repo/on544E/images/567738139-servidor-paquete.png)


* **APP**: Este paquete contiene las clases para el manejo de las conexiones con los clientes. En ella es donde se encuentra la clase principal encargada de aceptar las conexiones con los clientes y crear su respectivo hilo para su comunicación.
* **Factory**: En este paquete se encuentra implementado el patrón de diseño factory para la utilización de handlers, cada handler se encarga de procesar una petición diferente por parte del cliente.
* **Handlers**: Paquete donde se tiene toda la lógica de la aplicación por medio de hanlders para los diferentes comandos como lo son Crear Sala, Salir Sala, etc.

## Cliente

El cliente chat es desarrollado en java  y esta compuesto por los siguientes  paquetes.

![cliente-paquetes.png](https://bitbucket.org/repo/on544E/images/401321511-cliente-paquetes.png)

* **APP**:  En este paquete contiene la lógica de la aplicación, contiene las clases necesarias para autenticarse con el servidor, adicionalmente de crear los hilos que permiten la escritura y la lectura de datos con el servidor transaccional.
* **MESSAGES**: En este paquete están las clases necesarias para la construcción de mensajes que son enviados al servidor, desde su empaquetamiento y desempaquetamiento.

## Diagrama de Actividades

![diagrama-actividades.png](https://bitbucket.org/repo/on544E/images/1735209111-diagrama-actividades.png)


## Lista de Comandos desarrollados

La siguientes lista presenta los comandos para la utilización del chat:


```
#!java
* #cR <Nombre Sala> : Crear sala con el nombreSala. El servidor de forma automática ingresa a este cliente a la sala que creo.
* #gR <Nombre Sala> : Entrar a la sala nombreSala.
* #eR : Salir de la sala en que se encuentra. El servidor enviará al cliente a la sala por defecto. Si el cliente ingresa este comando estando en la sala por defecto, no tendrá ningún efecto.
* #exit: Desconectará al cliente del servidor
* #1R : Lista los nombres de todas las sala disponibles y el número de participantes de cada una.
* #dR <nombreSala>. Elimina la sala nombreSala. Un cliente solo puede eliminar las salas que creo

```





## Ejecución del programa


Requerimientos: Es necesario tener instalada como mínimo la versión de java 1.7( JRE 1.7.0).

###Ejecución Servidor:

Entre a la carpeta chat-servidor y configure el puerto por medio del archivo servidor.properties.
Ejecute el archivo llamado chat-servidor.bat

![servidor-img.png](https://bitbucket.org/repo/on544E/images/750828897-servidor-img.png)

###Ejecución Cliente:

Entre a la carpeta chat-cliente y configure los datos de conexion en el archivo cliente.properties.
Ejecute el archivo llamado chat-cliente.bat
Ingrese datos de usuario y contraseña

![cliente-img.png](https://bitbucket.org/repo/on544E/images/2617704509-cliente-img.png)

Puede ejecutar tantos cliente como requiera.
