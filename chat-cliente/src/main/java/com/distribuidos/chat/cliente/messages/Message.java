package com.distribuidos.chat.cliente.messages;

/**
 * Clase base abstracta de todos los mensajes de la aplicacion. El cual expone
 * dos metodos. Uno para la consturcion del mensaje de salida y otro para
 * realizar el paseo del mensaje de entrada-
 * 
 * @author Paola
 * @author Juan David
 */
public abstract class Message {

	/** Indica si la transacción es exitosa */
	private boolean estado;

	/** Método para la construccion de los mensjaes de salida. */
	public abstract String build();

	/** Método para el parseo del mensaje de entrada. */
	public abstract void parse(String respuesta);

	/**
	 * @return the estado
	 */
	public boolean isEstado() {
		return estado;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(boolean estado) {
		this.estado = estado;
	}

}
