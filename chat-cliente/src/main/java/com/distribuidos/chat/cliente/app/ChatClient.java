package com.distribuidos.chat.cliente.app;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase ChatClient. Contiene la lógica para inicializar los servicios de
 * escritura y lectura para el chat.
 * 
 * @author Paola
 * @author Juan David
 */
public class ChatClient {
	
	public static boolean estado = true;

	public void ejecutar() throws IOException {
		Socket socket = null;
		PrintWriter socketOut = null;
		BufferedReader socketIn = null;
		try {
			System.out.println("Proyecto - CHAT - Sistemas Distribuidos");

			/*************************************************************
			 * Se obtiene los datos de conexion del archivo de propiedades
			 *************************************************************/
			Properties prop = new Properties();
			prop.load(new FileInputStream("cliente.properties"));

			String ip = prop.getProperty("ipServidor");
			int puerto = Integer.valueOf(prop.getProperty("puertoServidor"));
			
			System.out.println("Datos de Conexion: ip:" + ip + " , puerto:" + puerto) ;

			
			/******************************************
			 * Se establece la conexión con el servidor.
			 ******************************************/
			InetAddress servidorIp = InetAddress.getByName(ip);
			socket = new Socket(servidorIp, puerto);
			socketOut = new PrintWriter(socket.getOutputStream(),	true);
			socketIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
			/***********************************
			 *  Primero se debe iniciar sesion
			 *********************************/
			while (true) {
				if (ChatClientLogin.login(socketOut, socketIn)) {
					break;
				}
			}

			/*****************************************************************************
			 * Se crean los hilos que permiten la lectura  y escritura de datos al socket.
			 ******************************************************************************/
			ChatClientListener listener = new ChatClientListener(socketIn);
			ChatClientWriter writer = new ChatClientWriter(socketOut);
			listener.start();
			writer.start();
			
			listener.join();
			writer.join();
			
			System.out.println("Usted se ha desconectado...");
		} catch (Exception ex) {
			Logger.getLogger(ChatClient.class.getName()).log(Level.SEVERE,	null, ex);
		} finally{
			
			if(socketOut != null){
				socketOut.close();
			}
			if(socketIn != null){
				socketIn.close();
			}
			if(socket != null){
				socket.close();
			}
		}
	}

}
