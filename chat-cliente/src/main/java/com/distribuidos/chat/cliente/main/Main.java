package com.distribuidos.chat.cliente.main;

import java.io.IOException;

import com.distribuidos.chat.cliente.app.ChatClient;

/**
 * Clase Main de la aplicación. Se instancia un objeto de tipo ChatClient el
 * cual contiene la lógica para el chat.
 * 
 * @author Paola
 * @author Juan David
 */
public class Main {
	public static void main(String[] args) throws IOException {
		ChatClient chatCliente = new ChatClient();
		chatCliente.ejecutar();
	}

}
