package com.distribuidos.chat.cliente.messages;

/**
 * Clase LoginMessage. Clase utilizada para la construcción de los mensajes de
 * salida.
 * 
 * @author Paola
 * @author Juan David
 */
public class LoginMessage extends Message {

	/** Codigo de la trama de LOGIN*/
	public static final String CODE = "#LO";

	/** Usuario para realizar la autenticacíon*/
	private String usuario;
	
	/** Password para realizar la autenticacíon*/
	private String password;

	/** Mensaje de respuesta*/
	private String mensaje;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.distribuidos.chat.cliente.messages.Message#build()
	 */
	@Override
	public String build() {
		return CODE + "|" + usuario + "&" + password;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.distribuidos.chat.cliente.messages.Message#parse(java.lang.String)
	 */
	@Override
	public void parse(String respuesta) {
		String[] array = respuesta.split("\\|");
		if ("0".equals(array[1])) {
			setEstado(true);
		} else {
			setEstado(false);
		}
		setMensaje(array[2]);
	}

	

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario
	 *            the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}


	/**
	 * @return the mensaje
	 */
	public String getMensaje() {
		return mensaje;
	}

	/**
	 * @param mensaje the mensaje to set
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}


}
