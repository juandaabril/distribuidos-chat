package com.distribuidos.chat.cliente.app;

import java.io.PrintWriter;
import java.util.Scanner;

import com.distribuidos.chat.cliente.messages.DefaultMessage;

/**
 * Clase ChatClientWriter. Encargada de pedir los mnesajes por consola para ser
 * enviados al servidor.
 * 
 * @author Paola
 * @author Juan David
 */
public class ChatClientWriter extends Thread {

	/** Referencia al buffer de entrada */
	private PrintWriter socketOut;

	public ChatClientWriter(PrintWriter socketOut) {
		this.socketOut = socketOut;

	}

	public void run() {
		Scanner sc = new Scanner(System.in);
		DefaultMessage dm = new DefaultMessage();
		while (ChatClient.estado) {
			dm.setTexto(sc.nextLine());
			socketOut.write(dm.build() + "\n");
			socketOut.flush();
		}

	}

}
