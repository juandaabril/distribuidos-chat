package com.distribuidos.chat.cliente.messages;

import com.distribuidos.chat.cliente.app.ChatClient;

/**
 * 
 * Clase DefaultMessage. Permite la construcción de los mensajes que se serán
 * enviados al servidor.
 * 
 * @author Paola
 * @author Juan David
 */
public class DefaultMessage extends Message {
	
	/** Mensaje a enviar el servidor */
	private String texto;
	
	/** Respuesta del servidor*/
	private String respuesta;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.distribuidos.chat.cliente.messages.Message#build()
	 */
	@Override
	public String build() {
		if (texto.startsWith("#")) {
			String comando = texto.substring(0, 3);
			String trama = texto.substring(3).trim();
			if("#exit".equals(texto)){
				ChatClient.estado = false; 
				comando = "#ex";
				trama = "";
			}
			
			return comando + "|" + trama;
		} else {
			return "#TX|" + texto;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.distribuidos.chat.cliente.messages.Message#
	 * parse(java.lang.String)
	 */
	@Override
	public void parse(String respuesta) {
		/**/
	}

	/**
	 * @return the texto
	 */
	public String getTexto() {
		return texto;
	}

	/**
	 * @param texto
	 *            the texto to set
	 */
	public void setTexto(String texto) {
		this.texto = texto;
	}

	/**
	 * @return the respuesta
	 */
	public String getRespuesta() {
		return respuesta;
	}

	/**
	 * @param respuesta
	 *            the respuesta to set
	 */
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

}
