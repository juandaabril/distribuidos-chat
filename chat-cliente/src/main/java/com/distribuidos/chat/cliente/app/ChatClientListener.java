package com.distribuidos.chat.cliente.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase ChatClientListener. Encargada de estar escuchando los mensajes que son enviados desde el servidor. 
 * @author Paola
 * @author Juan David
 */
public class ChatClientListener extends Thread {

	/** Referencia al buffer de entrada*/
    private BufferedReader socketIn;

    public ChatClientListener(BufferedReader socketIn) {
        this.socketIn = socketIn;
    }

    public void run() {
        try {
            while (ChatClient.estado) {
                String respuesta = socketIn.readLine();
                if(respuesta == null || respuesta.isEmpty()){
                    continue;
                }
                String array[] = respuesta.split("\\|");
                if(array.length>2){
                     System.out.println("\n" + array[2]);
                }
               
            }
        } catch (IOException ex) {
            Logger.getLogger(ChatClientListener.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
