package com.distribuidos.chat.cliente.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.distribuidos.chat.cliente.messages.LoginMessage;

/**
 * Clase que expone un método estático para realizar la autenticación con el
 * servidor chat.
 * 
 * 
 * @author Paola
 * @author Juan David
 */
public class ChatClientLogin {

	/**
	 * Método para realizar la autenticación con el servidor.
	 * 
	 * @param socketOut
	 *            Buffer de salida
	 * @param socketIn
	 *            Buffer de entrada
	 * @return True o False. Si esta autenticado true. De lo contrario false
	 */
	public static boolean login(PrintWriter socketOut, BufferedReader socketIn) {
		try {
			Scanner sc = new Scanner(System.in);

			/**********************************************
			 * Se obtienen los datos desde consola
			 *********************************************/
			System.out.println("Usuario:");
			String usuario = sc.nextLine();
			System.out.println("Password:");
			String password = sc.nextLine();

			/**********************************************
			 * Se crea el mensaje de Login
			 *********************************************/
			LoginMessage loginMessage = new LoginMessage();
			loginMessage.setUsuario(usuario);
			loginMessage.setPassword(password);

			/*********************************************
			 * Se envia la trama de LOGIN por el socket
			 ******************************************* */
			socketOut.write(loginMessage.build() + "\n");
			socketOut.flush();

			String respuesta = socketIn.readLine();
			loginMessage.parse(respuesta);
			
			/*********************************************
			 * Se evalua la respuesta del servidor
			 ******************************************* */
			System.out.println(loginMessage.getMensaje());
			if (loginMessage.isEstado()) {
				return true;
			}

			return false;
		} catch (IOException ex) {
			Logger.getLogger(ChatClientLogin.class.getName()).log(Level.SEVERE,	null, ex);
			return false;
		}
	}

}
