package com.distribuidos.chat.servidor.factory.handlers;

import java.util.List;
import java.util.Map;

import com.distribuidos.chat.servidor.app.ClienteChat;
import com.distribuidos.chat.servidor.app.MensajeChat;
import com.distribuidos.chat.servidor.app.ServidorChat;
import com.distribuidos.chat.servidor.factory.Handler;

/**
 * Clase encargada de enviar un texto a los otros usuarios de la sala
 *
 * @author Paola
 * @author Juan David
 */
public class TextoHandler extends Handler {

	/** Codigo del comando */
    public static final String CODE = "#TX";

    /*
     * (non-Javadoc)
     * @see com.distribuidos.chat.servidor.factory.Handler#
     * execute(com.distribuidos.chat.servidor.app.ServidorChat, 
     * com.distribuidos.chat.servidor.app.ClienteChat, 
     * com.distribuidos.chat.servidor.app.MensajeChat)
     */
    @Override
    public void execute(ServidorChat servidorChat, ClienteChat clienteChat, MensajeChat mensajeChat) {
    	
        /****************************************
         * Si no se envio mensaje no se hace nada
         ***************************************/
    	if(mensajeChat.getMensaje() == null){
    		return;
    	}
    	
        /************************************
         * Mensjae a enviar por el socket
         *********************************/
        String mensaje = clienteChat.getUsuario() + ":" + mensajeChat.getMensaje();
        
        /******************************************************
         * Se obtiene la sala en la que se encuentra el cliente
         *****************************************************/
        String sala = servidorChat.getSalaPorCliente(clienteChat.getUsuario());
        Map<String, List<ClienteChat>>  mapSala =  servidorChat.getMapSalas();        
        List<ClienteChat> listaClientes = mapSala.get(sala);
        
        /*********************************************************
         * Se recorren todos los clientes y se le envia el mensaje
         *********************************************************/
        for (ClienteChat cliente : listaClientes) {
            if (cliente.getUsuario().equals(clienteChat.getUsuario())) {
                continue;
            }
            mensajeChat.setRespuesta(mensaje);
            cliente.enviarMensaje(mensajeChat);
        }

    }

}
