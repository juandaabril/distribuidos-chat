package com.distribuidos.chat.servidor.app;

/**
 * Clase MensajeChat. Clase encargada de recibir las peticiones y construir el
 * mensaje de respuesta.
 * 
 * @author Paola
 * @author Juan David
 */
public class MensajeChat {

	private String code;
	private String mensaje;
	private String respuesta;
	private boolean estado;
	
	/** Método para obtener el CODIGO del handler enviado en la trama. */
	public static MensajeChat get(String trama) {
		String[] token = trama.split("\\|");
		MensajeChat chatMessage = new MensajeChat();
		chatMessage.setCode(token[0]);
		if (token.length > 1) {
			chatMessage.setMensaje(token[1]);
		}
		return chatMessage;
	}

	/** Método utilizado para la construcción de la trama de respuesta. */
	public String getTrama() {
		return code + "|" + ((estado) ? "0" : "1") + "|" + respuesta;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the mensaje
	 */
	public String getMensaje() {
		return mensaje;
	}

	/**
	 * @param mensaje
	 *            the mensaje to set
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	/**
	 * @return the respuesta
	 */
	public String getRespuesta() {
		return respuesta;
	}

	/**
	 * @param respuesta
	 *            the respuesta to set
	 */
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	/**
	 * @return the estado
	 */
	public boolean isEstado() {
		return estado;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(boolean estado) {
		this.estado = estado;
	}

}
