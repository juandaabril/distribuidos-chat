package com.distribuidos.chat.servidor.main;

import com.distribuidos.chat.servidor.app.ServidorChat;

/**
 * Clase Main de la aplicación. Se instancia un objeto de tipo ServidorChat el
 * cual contiene la lógica para el chat.
 * 
 * @author Paola
 * @author Juan David
 */
public class Main {

	public static void main(String[] args) {
		ServidorChat chatServer = new ServidorChat();
		chatServer.ejecutar();
	}

}
