package com.distribuidos.chat.servidor.factory.handlers;

import java.util.List;

import com.distribuidos.chat.servidor.app.ClienteChat;
import com.distribuidos.chat.servidor.app.MensajeChat;
import com.distribuidos.chat.servidor.app.ServidorChat;
import com.distribuidos.chat.servidor.factory.Handler;

/**
 * Clase encargada de para salir de una sala
 * 
 * @author Paola
 * @author Juan David
 */
public class EliminarSalaHandler extends Handler {

	/** Codigo del comando */
    public static final String CODE = "#dR";

    /*
     * (non-Javadoc)
     * @see com.distribuidos.chat.servidor.factory.Handler#
     * execute(com.distribuidos.chat.servidor.app.ServidorChat, 
     * com.distribuidos.chat.servidor.app.ClienteChat, 
     * com.distribuidos.chat.servidor.app.MensajeChat)
     */
    @Override
    public void execute(ServidorChat servidorChat, ClienteChat clienteChat, MensajeChat mensajeChat) {
        /*******************************************************
         * Se obtiene el nombre de la sala que se desea eliminar
         ******************************************************/
        String salaEliminar = mensajeChat.getMensaje();
        
        /****************************************
         * Si no se envio mensaje no se hace nada
         ***************************************/
    	if(salaEliminar == null){
    		return;
    	}
    	
        String salaCliente = servidorChat.getMapClienteSala().get(clienteChat.getUsuario());
        String nuevaSala = "default";
    	
        /************************************************************
         * Se verifica que el usuario sea la persona que creo la sala
         ************************************************************/
        if(!salaEliminar.equals(salaCliente)){
            mensajeChat.setRespuesta("La sala fue creada por otra persona. No se puede eliminar.");
            mensajeChat.setEstado(false);
            clienteChat.enviarMensaje(mensajeChat);
            return;
        	
        }
        
        /************************************************************
         * Se verifica si no hay usuarios en la sala
         ************************************************************/
        if(servidorChat.getMapSalas().get(salaEliminar).size() >1){
            mensajeChat.setRespuesta("Hay mas de un cliente conectado a la sala. No se puede eliminar");
            mensajeChat.setEstado(false);
            clienteChat.enviarMensaje(mensajeChat);
            return;
        }
        
        
        /*********************************************************
         * Se debe eliminar al usuario de la sala en la que estaba
         *********************************************************/
        List<ClienteChat> listaCliente = servidorChat.getMapSalas().get(salaEliminar);
        listaCliente.remove(clienteChat);
        
        /*************************************************
         * Si la lista esta vacia se debe eliminar la sala
         *************************************************/
        if(listaCliente.isEmpty() && !"default".equals(salaEliminar)){
            servidorChat.getMapSalas().put(salaEliminar, null);
        }
                 
        /***************************
         * Se añade a la nueva sala        
         ***************************/
        servidorChat.getMapSalas().get(nuevaSala).add(clienteChat);
        servidorChat.getMapClientes().put(clienteChat.getUsuario(), nuevaSala);
        servidorChat.getMapClienteSala().put(clienteChat.getUsuario(), null);
        
       /****************************
        *Se crea trama de respuesta
        ****************************/
        mensajeChat.setRespuesta("Bienvenido a la sala: " + nuevaSala);
        mensajeChat.setEstado(true);
        clienteChat.enviarMensaje(mensajeChat);        
      
    }

}
