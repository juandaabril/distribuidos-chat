package com.distribuidos.chat.servidor.factory.handlers;

import java.util.ArrayList;
import java.util.List;

import com.distribuidos.chat.servidor.app.ClienteChat;
import com.distribuidos.chat.servidor.app.MensajeChat;
import com.distribuidos.chat.servidor.app.ServidorChat;
import com.distribuidos.chat.servidor.factory.Handler;

/**
 * Clase encarga de realizar el login del usuario
 *
 * @author Paola
 * @author Juan David
 */
public class LoginHandler extends Handler {

	/** Codigo del comando */
    public static final String CODE = "#LO";

    @Override
    public void execute(ServidorChat servidorChat, ClienteChat clienteChat, MensajeChat mensajeChat) {
    	
    	/**********************************************
    	 * Se obtienen los datos que vienen en la trama
    	 **********************************************/
        String array[] = mensajeChat.getMensaje().split("&");
        String usuario = array[0];
        String password = array[1];
        
        /************************************************************************
         * Se valida que el nombre del usuario no este utilizado por otro usuario
         ************************************************************************/
        if(servidorChat.getMapClientes().get(usuario) != null){
            mensajeChat.setRespuesta("El nombre de usuario '" + usuario + "' ya se encuentra en el sistema");
            mensajeChat.setEstado(false);
            clienteChat.enviarMensaje(mensajeChat);
            return;
        }
        
        
        clienteChat.setUsuario(usuario);
        
        /*************************************************************
         * NO SE REALIZA VALIDACION CONTRA ALGUNA CAPA DE PERSISTENCIA
         * POR LO TANTO SE INICIA SESION CON CUALQUIER INFORMACION
         ***************************************************************/
        
        /***************************************
         * Se añade el cliente a la sala default
         ***************************************/
        List<ClienteChat> listaCliente = servidorChat.getMapSalas().get("default");

        if (listaCliente == null) {
            listaCliente = new ArrayList<ClienteChat>();
            servidorChat.getMapSalas().put("default", listaCliente);
        }                    
        servidorChat.getMapClientes().put(usuario, "default");
        listaCliente.add(clienteChat);

        /******************************
         * Se crea trama de respuesta
         *****************************/
        mensajeChat.setRespuesta("Bienvenido a la sala: default");
        mensajeChat.setEstado(true);
        clienteChat.enviarMensaje(mensajeChat);
    }

}
