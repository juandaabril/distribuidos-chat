package com.distribuidos.chat.servidor.factory;

import java.util.HashMap;
import java.util.Map;

import com.distribuidos.chat.servidor.app.MensajeChat;
import com.distribuidos.chat.servidor.factory.handlers.CrearSalaHandler;
import com.distribuidos.chat.servidor.factory.handlers.EliminarSalaHandler;
import com.distribuidos.chat.servidor.factory.handlers.ExitServerHandler;
import com.distribuidos.chat.servidor.factory.handlers.ListarSalasHandler;
import com.distribuidos.chat.servidor.factory.handlers.LoginHandler;
import com.distribuidos.chat.servidor.factory.handlers.SalirSalaHandler;
import com.distribuidos.chat.servidor.factory.handlers.TextoHandler;
import com.distribuidos.chat.servidor.factory.handlers.UnirSalaHandler;

/**
 * Clase FACTORY para obtener la creación y obtención a las referencias de cada
 * uno de los handlers disponibles.
 * 
 * @author Paola
 * @author Juan David
 */
public class HandlerFactory {

	/** Map donde se encuentran los handlres disponibles*/
	private static Map<String, Handler> map;

	static {
		map = new HashMap<String, Handler>();
		map.put(LoginHandler.CODE, new LoginHandler());
		map.put(TextoHandler.CODE, new TextoHandler());
		map.put(CrearSalaHandler.CODE, new CrearSalaHandler());
		map.put(UnirSalaHandler.CODE, new UnirSalaHandler());
		map.put(SalirSalaHandler.CODE, new SalirSalaHandler());
		map.put(ListarSalasHandler.CODE, new ListarSalasHandler());
		map.put(EliminarSalaHandler.CODE, new EliminarSalaHandler());
		map.put(ExitServerHandler.CODE, new ExitServerHandler());
	}

	/** Método para obtener el handler dependiendo del codigo*/
	public static Handler get(MensajeChat chatMessage) {
		return map.get(chatMessage.getCode());
	}

}
