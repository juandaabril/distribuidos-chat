package com.distribuidos.chat.servidor.app;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase ServidorChat. Clase principal el cual es el encargada de estar
 * escuchando por las conexiones entrante y de crear un hilo para la
 * administración de cada cliente.
 * 
 * @author Paola
 * @author Juan David
 */
public class ServidorChat {

	/** Referencia al socketServer*/
	private ServerSocket server;
	
	/** Map con la lista de clientes para cada sala*/
	private Map<String, List<ClienteChat>> mapSalas;
	
	/** Map con los clientes y en la sala en la que se encuentra*/
	private Map<String, String> mapClientes;
	
	/** Map con los clientes y en la sala de la cual es dueño*/
	private Map<String, String> mapClienteSala;

	public void ejecutar() {
		try {
			
			System.out.println("Proyecto - CHAT - Sistemas Distribuidos");

			/*************************************************************
			 * Se obtiene los datos de conexion del archivo de propiedades
			 *************************************************************/
			Properties prop = new Properties();
			prop.load(new FileInputStream("servidor.properties"));
			int puerto = Integer.valueOf(prop.getProperty("puertoServidor"));
			System.out.println("Datos de Conexion: puerto:" + puerto) ;
			
			/**********************************
			 * Se establece el servidor socket
			 ***********************************/
			
			server = new ServerSocket(puerto);
			mapSalas = new HashMap<String, List<ClienteChat>>();
			mapClientes = new HashMap<String, String>();
			mapClienteSala = new HashMap<String, String>();
			
			System.out.println("Ejecutandose");

			while (true) {
				System.out.println("Escuchando ... Esperando clientes");
				Socket socket = server.accept();
				ClienteChat chatConexion = new ClienteChat(socket, this);
				chatConexion.start();
			}

		} catch (IOException ex) {
			Logger.getLogger(ServidorChat.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/***
	 * Método para obtener la sala en la que se encuentra un cliente
	 * @param cliente
	 * @return
	 */
	public String getSalaPorCliente(String cliente) {
		return this.getMapClientes().get(cliente);
	}

	/**
	 * @return the mapClientes
	 */
	public Map<String, String> getMapClientes() {
		return mapClientes;
	}

	/**
	 * @param mapClientes
	 *            the mapClientes to set
	 */
	public void setMapClientes(Map<String, String> mapClientes) {
		this.mapClientes = mapClientes;
	}

	/**
	 * @return the mapSalas
	 */
	public Map<String, List<ClienteChat>> getMapSalas() {
		return mapSalas;
	}

	/**
	 * @param mapSalas
	 *            the mapSalas to set
	 */
	public void setMapSalas(Map<String, List<ClienteChat>> mapSalas) {
		this.mapSalas = mapSalas;
	}

	/**
	 * @return the mapClienteSala
	 */
	public Map<String, String> getMapClienteSala() {
		return mapClienteSala;
	}

	/**
	 * @param mapClienteSala the mapClienteSala to set
	 */
	public void setMapClienteSala(Map<String, String> mapClienteSala) {
		this.mapClienteSala = mapClienteSala;
	}

}
