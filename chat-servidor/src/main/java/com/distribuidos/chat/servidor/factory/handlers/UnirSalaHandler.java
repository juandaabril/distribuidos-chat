package com.distribuidos.chat.servidor.factory.handlers;

import java.util.List;

import com.distribuidos.chat.servidor.app.ClienteChat;
import com.distribuidos.chat.servidor.app.MensajeChat;
import com.distribuidos.chat.servidor.app.ServidorChat;
import com.distribuidos.chat.servidor.factory.Handler;

/**
 * Clase para uniser a una sala que se encuentra creada
 *
 * @author Paola
 * @author Juan David
 */
public class UnirSalaHandler extends Handler {

	/** Codigo del comando */
    public static final String CODE = "#gR";

    /*
     * (non-Javadoc)
     * @see com.distribuidos.chat.servidor.factory.Handler#
     * execute(com.distribuidos.chat.servidor.app.ServidorChat, 
     * com.distribuidos.chat.servidor.app.ClienteChat, 
     * com.distribuidos.chat.servidor.app.MensajeChat)
     */
    @Override
    public void execute(ServidorChat servidorChat, ClienteChat clienteChat, MensajeChat mensajeChat) {
        /*********************************
         * Se obtiene el nombre de la sala
         *********************************/
        String nuevaSala = mensajeChat.getMensaje();

        /**********************************
         * Se valida que la sala no exista
         **********************************/
        if (servidorChat.getMapSalas().get(nuevaSala) == null) {
            mensajeChat.setEstado(false);
            mensajeChat.setRespuesta("No existe una sala con ese nombre");
            clienteChat.enviarMensaje(mensajeChat);
            return;
        }        
        
        /***************************************************
         * Obtiene la sala en la que se encuentra el cliente
         ***************************************************/
        String viejaSala = servidorChat.getSalaPorCliente(clienteChat.getUsuario());

        /*********************************************************
         * Se debe eliminar al usuario de la sala en la que estaba
         **********************************************************/
        List<ClienteChat> listaCliente = servidorChat.getMapSalas().get(viejaSala);
        listaCliente.remove(clienteChat);
        
        /****************************
         * Se añade a la nueva sala        
         ***************************/
        servidorChat.getMapSalas().get(nuevaSala).add(clienteChat);
        servidorChat.getMapClientes().put(clienteChat.getUsuario(), nuevaSala);
        
        /****************************
         * Se crea trama de respuesta
         *****************************/
        mensajeChat.setRespuesta("Bienvenido a la sala: " + nuevaSala);
        mensajeChat.setEstado(true);
        clienteChat.enviarMensaje(mensajeChat); 
    }

}
