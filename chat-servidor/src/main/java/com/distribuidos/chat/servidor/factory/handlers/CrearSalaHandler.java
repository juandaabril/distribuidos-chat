package com.distribuidos.chat.servidor.factory.handlers;

import java.util.ArrayList;
import java.util.List;

import com.distribuidos.chat.servidor.app.ClienteChat;
import com.distribuidos.chat.servidor.app.MensajeChat;
import com.distribuidos.chat.servidor.app.ServidorChat;
import com.distribuidos.chat.servidor.factory.Handler;

/**
 * Clase encargada de crear una nueva sala para el usuario.
 *
 * @author Paola
 * @author Juan David
 */
public class CrearSalaHandler extends Handler {

	/** Codigo del comando*/
    public static final String CODE = "#cR";

    /*
     * (non-Javadoc)
     * @see com.distribuidos.chat.servidor.factory.Handler#
     * execute(com.distribuidos.chat.servidor.app.ServidorChat, 
     * com.distribuidos.chat.servidor.app.ClienteChat, 
     * com.distribuidos.chat.servidor.app.MensajeChat)
     */
    @Override
    public void execute(ServidorChat servidorChat, ClienteChat clienteChat, MensajeChat mensajeChat) {
        /*********************************
         * Se obtiene el nombre de la sala
         *********************************/
        String nuevaSala = mensajeChat.getMensaje();

        /*********************************
         * Se valida que la sala no exista
         *********************************/
        if (servidorChat.getMapSalas().get(nuevaSala) != null) {
            mensajeChat.setEstado(false);
            mensajeChat.setRespuesta("Ya hay una sala con ese nombre.");
            clienteChat.enviarMensaje(mensajeChat);
            return;                   
        }

        /****************************************************
         * Obtiene la sala en la que se encuentra el cliente
         ****************************************************/
        String viejaSala = servidorChat.getSalaPorCliente(clienteChat.getUsuario());

        /*********************************************************
         * Se debe eliminar al usuario de la sala en la que estaba
         *********************************************************/
        List<ClienteChat> listaCliente = servidorChat.getMapSalas().get(viejaSala);
        listaCliente.remove(clienteChat);

        /***************************
         * se añade a la nueva sala        
         **************************/
        listaCliente = new ArrayList<ClienteChat>();
        listaCliente.add(clienteChat);
        servidorChat.getMapSalas().put(nuevaSala, listaCliente);
        servidorChat.getMapClientes().put(clienteChat.getUsuario(), nuevaSala);
        servidorChat.getMapClienteSala().put(clienteChat.getUsuario(), nuevaSala);
        
        /****************************
         * Se crea trama de respuesta
         ****************************/
        mensajeChat.setRespuesta("Bienvenido a la sala: " + nuevaSala);
        mensajeChat.setEstado(true);
        clienteChat.enviarMensaje(mensajeChat);
    }
}
