package com.distribuidos.chat.servidor.factory.handlers;

import java.util.List;

import com.distribuidos.chat.servidor.app.ClienteChat;
import com.distribuidos.chat.servidor.app.MensajeChat;
import com.distribuidos.chat.servidor.app.ServidorChat;
import com.distribuidos.chat.servidor.factory.Handler;

/**
 * Clase encargada de para salir de una sala
 * 
 * @author Paola
 * @author Juan David
 */
public class SalirSalaHandler extends Handler {

	/** Codigo del comando */
    public static final String CODE = "#eR";

    /*
     * (non-Javadoc)
     * @see com.distribuidos.chat.servidor.factory.Handler#
     * execute(com.distribuidos.chat.servidor.app.ServidorChat, 
     * com.distribuidos.chat.servidor.app.ClienteChat, 
     * com.distribuidos.chat.servidor.app.MensajeChat)
     */
    @Override
    public void execute(ServidorChat servidorChat, ClienteChat clienteChat, MensajeChat mensajeChat) {
    	
    	/***************************************************
    	 * Obtiene la sala en la que se encuentra el cliente
    	 ***************************************************/
        String viejaSala = servidorChat.getSalaPorCliente(clienteChat.getUsuario());
        String nuevaSala = "default";
        
        /**********************************************************************
         * Si el usuario se encuentra en la sala DEFAULT no se debe hacer nada
         **********************************************************************/
        if ("default".equals(viejaSala)) {
            return;
        }

        /*********************************************************
         * Se debe eliminar al usuario de la sala en la que estaba
         *********************************************************/
        List<ClienteChat> listaCliente = servidorChat.getMapSalas().get(viejaSala);
        listaCliente.remove(clienteChat);
        
        /*************************************************
         * Si la lista esta vacia se debe eliminar la sala
         *************************************************/
        if(listaCliente.isEmpty() && !"default".equals(viejaSala)){
            servidorChat.getMapSalas().put(viejaSala, null);
        }
                 
        /***************************
         * Se añade a la nueva sala        
         ***************************/
        servidorChat.getMapSalas().get(nuevaSala).add(clienteChat);
        servidorChat.getMapClientes().put(clienteChat.getUsuario(), nuevaSala);
        
       /****************************
        *Se crea trama de respuesta
        ****************************/
        mensajeChat.setRespuesta("Bienvenido a la sala: " + nuevaSala);
        mensajeChat.setEstado(true);
        clienteChat.enviarMensaje(mensajeChat);        
    }

}
