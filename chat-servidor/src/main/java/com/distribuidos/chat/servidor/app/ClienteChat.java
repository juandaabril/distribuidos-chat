package com.distribuidos.chat.servidor.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.distribuidos.chat.servidor.factory.Handler;
import com.distribuidos.chat.servidor.factory.HandlerFactory;

/**
 * 
 * Clase ClienteChat. Encargada de gestionar las peticiones que son enviadas
 * desde el cliente.
 * 
 * @author Paola
 * @author Juan David
 */
public class ClienteChat extends Thread {

	/** Referencia al socket*/
	private Socket socket;
	
	/** Referencia al chatServer*/
	private ServidorChat chatServer;
	
	/** Referencia al buffer de entrada*/
	private BufferedReader socketIn;
	
	/** Referencia al buffer de salida*/
	private PrintWriter socketOut;
	
	/** Codigo del usuario*/
	private String usuario;
	
	/** Indica el estado de la transacción*/
	private boolean estado = true;

	public ClienteChat(Socket socket, ServidorChat chatServer) {
		this.socket = socket;
		this.chatServer = chatServer;
	}

	public void run() {
		try {
			/***********************************************
			 * Se obtienen los BUFFER de escritura y lectura
			 ***********************************************/
			socketIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			socketOut = new PrintWriter(socket.getOutputStream(), true);

			while (estado) {
				try{
					/***********************************************
					 * Se espera a que el cliente envie alguna trama
					 ***********************************************/
					String trama = socketIn.readLine();
					if (trama == null) {
						break;
					}
					System.out.println("TramaIn :" + trama);

					/*************************************************************
					 * Dependiendo del codigo de la trama se obtiene su respectivo
					 * HANDLER para responder la peticion del cliente
					 **************************************************************/
					MensajeChat mensajeChat = MensajeChat.get(trama);
					Handler handler = HandlerFactory.get(mensajeChat);
					if(handler!= null){
						handler.execute(chatServer, this, mensajeChat);
					}
					
				} catch(Exception ex){
					Logger.getLogger(ClienteChat.class.getName()).log(Level.SEVERE,	null, ex);
				}
			}
		} catch (IOException ex) {
			Logger.getLogger(ClienteChat.class.getName()).log(Level.SEVERE,	null, ex);
		} finally {
			try {
				if (socketOut != null) {
					socketOut.close();
				}

				if (socketIn != null) {
					socketIn.close();
				}

				if (socket != null) {
					socket.close();
				}
			} catch (IOException ex) {
				Logger.getLogger(ClienteChat.class.getName()).log(Level.SEVERE,
						null, ex);
			}
		}

	}

	/**
	 * Método para enviar un mensaje al cliente
	 * 
	 * @param mensaje
	 */
	public void enviarMensaje(MensajeChat mensajeChat) {
		System.out.println("TramaOut :" + mensajeChat.getTrama());
		socketOut.write(mensajeChat.getTrama() + "\n");
		socketOut.flush();
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario
	 *            the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the estado
	 */
	public boolean isEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(boolean estado) {
		this.estado = estado;
	}

}
