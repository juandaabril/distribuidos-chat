package com.distribuidos.chat.servidor.factory.handlers;

import java.util.List;
import java.util.Map;

import com.distribuidos.chat.servidor.app.ClienteChat;
import com.distribuidos.chat.servidor.app.MensajeChat;
import com.distribuidos.chat.servidor.app.ServidorChat;
import com.distribuidos.chat.servidor.factory.Handler;

/**
 * Clase encargada para listar las diferentes salas que se encuentran creadas.
 * 
 * @author Paola
 * @author Juan David
 */
public class ListarSalasHandler extends Handler {

	/** Codigo del comando */
	public static final String CODE = "#1R";

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.distribuidos.chat.servidor.factory.Handler#
	 * execute(com.distribuidos.chat.servidor.app.ServidorChat,
	 * com.distribuidos.chat.servidor.app.ClienteChat,
	 * com.distribuidos.chat.servidor.app.MensajeChat)
	 */
	@Override
	public void execute(ServidorChat servidorChat, ClienteChat clienteChat,
			MensajeChat mensajeChat) {
		
		/*********************************************************
		 * Se recorre cada una de las salas y se envian al cliente
		 ********************************************************/
		for (Map.Entry<String, List<ClienteChat>> entry : servidorChat.getMapSalas().entrySet()) {
			
			if(entry.getKey() == null || entry.getValue() == null){
				continue;
			}
			
			mensajeChat.setEstado(true);
			mensajeChat.setRespuesta("Sala:" + entry.getKey() + " Usuarios:" + entry.getValue().size());
			clienteChat.enviarMensaje(mensajeChat);
		}
	}
}
