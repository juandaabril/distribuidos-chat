package com.distribuidos.chat.servidor.factory;

import com.distribuidos.chat.servidor.app.ClienteChat;
import com.distribuidos.chat.servidor.app.MensajeChat;
import com.distribuidos.chat.servidor.app.ServidorChat;

/**
 * Clase abstracta que representa que expone los métodos necesarios para la
 * administración de las peticiones del servidor chat.
 * 
 * @author Paola
 * @author Juan David
 */
public abstract class Handler {

	/**
	 * Método para responder y procesar las peticiones enviadas desde cliente
	 * 
	 * @param servidorChat
	 *            Referencia al servidor chat
	 * @param clienteChat
	 *            Referencia al cliente que esta enviando la peticion
	 * @param mensajeChat
	 *            Información de la trama
	 */
	public abstract void execute(ServidorChat servidorChat,
			ClienteChat clienteChat, MensajeChat mensajeChat);

}
