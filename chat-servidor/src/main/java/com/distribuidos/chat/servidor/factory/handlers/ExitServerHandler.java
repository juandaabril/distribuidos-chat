package com.distribuidos.chat.servidor.factory.handlers;

import java.util.List;

import com.distribuidos.chat.servidor.app.ClienteChat;
import com.distribuidos.chat.servidor.app.MensajeChat;
import com.distribuidos.chat.servidor.app.ServidorChat;
import com.distribuidos.chat.servidor.factory.Handler;

/**
 * Clase para salir del chat
 * 
 * @author Paola
 * @author Juan David
 */
public class ExitServerHandler extends Handler {

	/** Codigo del comando */
    public static final String CODE = "#ex";

    /*
     * (non-Javadoc)
     * @see com.distribuidos.chat.servidor.factory.Handler#
     * execute(com.distribuidos.chat.servidor.app.ServidorChat, 
     * com.distribuidos.chat.servidor.app.ClienteChat, 
     * com.distribuidos.chat.servidor.app.MensajeChat)
     */
    @Override
    public void execute(ServidorChat servidorChat, ClienteChat clienteChat, MensajeChat mensajeChat) {
    	/***************************************************
    	 * Obtiene la sala en la que se encuentra el cliente
    	 ***************************************************/
        String viejaSala = servidorChat.getSalaPorCliente(clienteChat.getUsuario());
        
        /*********************************************************
         * Se debe eliminar al usuario de la sala en la que estaba
         *********************************************************/
        List<ClienteChat> listaCliente = servidorChat.getMapSalas().get(viejaSala);
        listaCliente.remove(clienteChat);
        
        /*************************************************
         * Si la lista esta vacia se debe eliminar la sala
         *************************************************/
        if(listaCliente.isEmpty() && !"default".equals(viejaSala)){
            servidorChat.getMapSalas().put(viejaSala, null);
        }
        
        /***************************************
         * Se elimina la información del cliente
         ***************************************/
        servidorChat.getMapClientes().put(clienteChat.getUsuario(), null);
        servidorChat.getMapClienteSala().put(clienteChat.getUsuario(), null);
        
        /*********************************
         * Se finaliza el hilo del cliente
         ***********************************/
        clienteChat.setEstado(false);
        /******************************
         * Se crea trama de respuesta
         *****************************/
        mensajeChat.setRespuesta("Adios, " + clienteChat.getUsuario() );
        mensajeChat.setEstado(true);
        clienteChat.enviarMensaje(mensajeChat);
    }

}
